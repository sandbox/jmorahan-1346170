<?php
// $Id$

define('UUID_PATH_METHOD_REWRITE', 0);
define('UUID_PATH_METHOD_ALIAS', 1);

/**
 * Implements hook_menu().
 */
function uuid_path_menu() {
  $items['admin/config/system/uuid_path'] = array(
    'title' => 'UUID path settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uuid_path_settings'),
    'access arguments' => array('administer site configuration'),
    'file' => 'uuid_path.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_uuid_path_patterns().
 */
function taxonomy_uuid_path_patterns() {
  return array(
    'taxonomy/term/%taxonomy_term' => t('Taxonomy terms'),
  );
}

/**
 * Implements hook_uuid_path_patterns().
 */
function comment_uuid_path_patterns() {
  return array(
    'comment/%comment' => t('Comments'),
    'comment/reply/%node/%comment' => t('Comment reply forms'),
  );
}

/**
 * Implements hook_uuid_path_patterns().
 */
function user_uuid_path_patterns() {
  return array(
    'user/%user' => t('Users'),
  );
}

/**
 * Implements hook_uuid_path_patterns().
 */
function node_uuid_path_patterns() {
  return array(
    'node/%node/revisions/%node_revision' => t('Node revisions'),
    'node/%node' => t('Nodes'),
  );
}

/**
 * Implements hook_uuid_path_db_info().
 */
function node_uuid_path_db_info() {
  return array(
    'node' => array('table' => 'node', 'id' => 'nid'),
    'node_revision' => array('table' => 'node_revision', 'id' => 'vid', 'uuid' => 'vuuid'),
  );
}

/**
 * Implements hook_uuid_path_db_info().
 */
function user_uuid_path_db_info() {
  return array(
    'user' => array('table' => 'users', 'id' => 'uid'),
  );
}

/**
 * Implements hook_uuid_path_db_info().
 */
function comment_uuid_path_db_info() {
  return array(
    'comment' => array('table' => 'comment', 'id' => 'cid'),
  );
}

/**
 * Implements hook_uuid_path_db_info().
 */
function taxonomy_uuid_path_db_info() {
  return array(
    'taxonomy_term' => array('table' => 'taxonomy_term_data', 'id' => 'tid'),
  );
}

/**
 * This needs to be a separate function for PHP 5.2.
 */
function _uuid_path_sort($a, $b) {
  return drupal_strlen($b) - drupal_strlen($a);
}

function _uuid_path_patterns($all = FALSE) {
  // Use the advanced drupal_static() pattern, since this is called very often.
  static $drupal_static_fast;
  if (!isset($drupal_static_fast)) {
    $drupal_static_fast['patterns'] = &drupal_static(__FUNCTION__);
  }
  $patterns = &$drupal_static_fast['patterns'];

  if (!isset($patterns)) {
    $patterns['all'] = module_invoke_all('uuid_path_patterns');
    // Sort by length desc, so that longer patterns come first and take precedence.
    uksort($patterns['all'], '_uuid_path_sort');
    $patterns['some'] = array_intersect_key($patterns, array_flip(variable_get('uuid_path_patterns', array())));
  }

  return $patterns[$all ? 'all' : 'some'];
}

/**
 * Implements hook_url_inbound_alter().
 */
function uuid_path_url_inbound_alter(&$path, $original_path, $path_language) {
  if ($new_path = _uuid_path_replace($path, 'uuid_is_valid', '_uuid_path_find_id')) {
    $path = $new_path;
  }
}

/**
 * Implements hook_url_outbound_alter().
 */
function uuid_path_url_outbound_alter(&$path, &$options, $original_path) {
  if (variable_get('uuid_path_method', UUID_PATH_METHOD_ALIAS) == UUID_PATH_METHOD_ALIAS) {
    if ($new_path = _uuid_path_replace($path, 'is_numeric', '_uuid_path_find_uuid')) {
      $path = $new_path;
    }
  }
}

function _uuid_path_replace($path, $validate, $lookup) {
  foreach (array_keys(_uuid_path_patterns()) as $pattern) {
    // Split the pattern into parts and try to find a match for each part.
    $pattern_parts = explode('/', $pattern);
    $path_parts = explode('/', $path);
    $alias_parts = array();
    for ($i = 0; isset($pattern_parts[$i]); ++$i) {
      if (!isset($path_parts[$i])) {
        // The path is too short.
        continue 2;
      }
      if (preg_match('/^%(\w+)$/', $pattern_parts[$i], $matches)) {
        // This part is a placeholder, so try to do a lookup.
        $type = $matches[1];
        if (!$validate($from_id = $path_parts[$i])) {
          // The path part failed validation.
          continue 2; // foreach
        }
        if (!($to_id = $lookup($from_id, $type))) {
          // We have a path match, but no entity found.
          continue 2;
        }
        $alias_parts[$i] = $to_id;
      }
      else {
        // Not a placeholder, so look for an exact match.
        if ($path_parts[$i] !== $pattern_parts[$i]) {
          continue 2; // foreach
        }
        $alias_parts[$i] = $path_parts[$i];
      }
    }

    // If we got this far, we didn't trigger a continue, so we must have found
    // a match.  There may be additional parts in $path_parts that aren't part
    // of the match.  This is fine, but we need to copy them into the alias.
    for (; isset($path_parts[$i]); ++$i) {
      $alias_parts[$i] = $path_parts[$i];
    }

    return implode('/', $alias_parts);
  }
}

function _uuid_path_db_info($type) {
  $info = module_invoke_all('uuid_path_db_info');
  return $info[$type] + array('uuid' => 'uuid');
}

function _uuid_path_find_uuid($id, $type) {
  if ($info = _uuid_path_db_info($type)) {
    return db_select($info['table'], ' t')
      ->fields('t', array($info['uuid']))
      ->condition($info['id'], $id)
      ->execute()
      ->fetchField();
  }
}

function _uuid_path_find_id($uuid, $type) {
  if ($info = _uuid_path_db_info($type)) {
    return db_select($info['table'], ' t')
      ->fields('t', array($info['id']))
      ->condition($info['uuid'], $uuid)
      ->execute()
      ->fetchField();
  }
}
