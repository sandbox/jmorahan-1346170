<?php
// $Id$

function uuid_path_settings($form, &$form_state) {
  $form['uuid_path_patterns'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Paths to rewrite'),
    '#options' => _uuid_path_patterns(TRUE),
    '#default_value' => variable_get('uuid_path_patterns', array()),
  );
  $form['uuid_path_method'] = array(
    '#type' => 'radios',
    '#title' => t('Rewriting method'),
    '#options' => array(
      UUID_PATH_METHOD_REWRITE => t('Only rewrite incoming URLs'),
      UUID_PATH_METHOD_ALIAS => t('Also rewrite outgoing URLs'),
    ),
    '#default_value' => variable_get('uuid_path_method', UUID_PATH_METHOD_ALIAS),
  );
  $form['array_filter'] = array('#type' => 'value', '#value' => TRUE);
  return system_settings_form($form, $form_state);
}
